alert("Hi! Please provide necessary details for the completion of my activity. Thank you!");

/*
	[FUNCTION W/ PROMPT]
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function details(){
		let fullName = prompt("Please enter your full name: ");
		let age = prompt("Please enter your age: ");
		let location = prompt("Please enter your address: ");

		console.log("Hello, " + fullName + "\nYou are " + age + " years old. \nYou live in " + location + ".");
	};

	details();

/*
	[FUNCTION TO DISPLAY]
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:
	function printFavoriteBandsOrArtists (){
		console.log("1. Paramore \n2. Bamboo \n3. Maroon 5 \n4. Ed Sheeran \n5. Kenny Rogers");
	};

	printFavoriteBandsOrArtists();

/*
	[FUNCTION TO DISPLAY]
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:
	function printFavoriteMovies(){
		console.log("1. The Godfather \nRotten Tomatoes Rating: 97%");
		console.log("2. The Godfather, Part II \nRotten Tomatoes Rating: 96%");
		console.log("3. Shawshank Redemption \nRotten Tomatoes Rating: 91%");
		console.log("4. To Kill a Mockingbird \nRotten Tomatoes Rating: 93%");
		console.log("5. Psycho \nRotten Tomatoes Rating: 96%");
	};

	printFavoriteMovies();

/*	
	[DEBUG]
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriend = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};


// console.log(friend1);
// console.log(friend2);
printFriend();